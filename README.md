This resource pack replaces Minecraft's colormap texture, which is often used to tint grass and leaves in different biomes. These will have more variety in different biomes; leaves will be orange/"dried" in the desert and taiga biomes, and have a blueish tint in cold mountains.

![A screenshot of the dried tree leaves near a desert biome.](./.images/dried.png)
